package com.epam.ms.songservice.persistence;

import java.util.Objects;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity(name = "Song")
@Table(name = "songs")
public class SongJpaEntity {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Integer id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "artist", nullable = false)
    private String artist;

    @Column(name = "album", nullable = false)
    private String album;

    @Column(name = "length", nullable = false)
    private String length;

    @Column(name = "resource_id", nullable = false, unique = true)
    private Integer resourceId;

    @Column(name = "year", nullable = false)
    private Integer year;

    public SongJpaEntity() {
    }

    public SongJpaEntity(
            Integer id,
            String name,
            String artist,
            String album,
            String length,
            Integer resourceId,
            Integer year
    ) {
        this.id = id;
        this.name = Objects.requireNonNull(name);
        this.artist = Objects.requireNonNull(artist);
        this.album = Objects.requireNonNull(album);
        this.length = Objects.requireNonNull(length);
        this.resourceId = Objects.requireNonNull(resourceId);
        this.year = Objects.requireNonNull(year);
    }

    public static SongJpaEntity newSong(
            String name,
            String artist,
            String album,
            String length,
            Integer resourceId,
            Integer year
    ) {
        return new SongJpaEntity(null, name, artist, album, length, resourceId, year);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public Integer getResourceId() {
        return resourceId;
    }

    public void setResourceId(Integer resourceId) {
        this.resourceId = resourceId;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }
}
