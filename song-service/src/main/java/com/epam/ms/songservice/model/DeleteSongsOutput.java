package com.epam.ms.songservice.model;

import java.util.List;

public record DeleteSongsOutput(
        List<Integer> ids
) {
}
