package com.epam.ms.songservice.persistence;

import org.springframework.data.repository.ListCrudRepository;

public interface SongRepository extends ListCrudRepository<SongJpaEntity, Integer> {
}
