package com.epam.ms.songservice.controllers;

import com.epam.ms.songservice.model.CreateSongOutput;
import com.epam.ms.songservice.model.CreateSongRequest;
import com.epam.ms.songservice.model.DeleteSongsOutput;
import com.epam.ms.songservice.persistence.SongJpaEntity;
import com.epam.ms.songservice.persistence.SongRepository;
import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "songs")
public class SongController {

    private static final Logger log = LoggerFactory.getLogger(SongController.class);

    private final SongRepository songRepository;

    public SongController(SongRepository songRepository) {
        this.songRepository = Objects.requireNonNull(songRepository);
    }

    @PostMapping
    ResponseEntity<?> createSongMetadata(@RequestBody CreateSongRequest request) {
        try {
            var newSong = SongJpaEntity.newSong(
                    request.name(),
                    request.artist(),
                    request.album(),
                    request.length(),
                    request.resourceId(),
                    request.year()
            );

            var newSongId = this.songRepository.save(newSong).getId();

            return ResponseEntity.ok(new CreateSongOutput(newSongId));
        } catch (NullPointerException ex) {
            log.error("Error to create new song", ex);
            return ResponseEntity.badRequest().build();
        }
    }

    @GetMapping(value = "{id}")
    ResponseEntity<?> getById(@PathVariable(name = "id") Integer id) {
        return this.songRepository.findById(id)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @DeleteMapping
    public ResponseEntity<DeleteSongsOutput> deleteResources(@RequestParam(name = "id") String stringIds) {
        var idsToDelete = Arrays.stream(stringIds.split(","))
                .map(Integer::valueOf)
                .collect(Collectors.toList());

        this.songRepository.deleteAllById(idsToDelete);

        return ResponseEntity.ok(new DeleteSongsOutput(idsToDelete));
    }
}
