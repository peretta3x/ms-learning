package com.epam.ms.songservice.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public record CreateSongRequest(
        @JsonProperty("name") String name,
        @JsonProperty("artist") String artist,
        @JsonProperty("album") String album,
        @JsonProperty("length") String length,
        @JsonProperty("resourceId") Integer resourceId,
        @JsonProperty("year") Integer year
) {
}
