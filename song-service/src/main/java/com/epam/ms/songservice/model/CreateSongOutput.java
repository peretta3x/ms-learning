package com.epam.ms.songservice.model;

public record CreateSongOutput(
        Integer id
) {
}
