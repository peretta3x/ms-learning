CREATE TABLE songs (
	id SERIAL PRIMARY KEY,
	name VARCHAR(255) NOT NULL,
	artist VARCHAR(255) NOT NULL,
	album VARCHAR(255) NOT NULL,
	length VARCHAR(15) NOT NULL,
	resource_id Integer NOT NULL unique,
	year Integer NOT NULL
);
