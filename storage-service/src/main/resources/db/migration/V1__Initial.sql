CREATE TABLE storages (
	id SERIAL PRIMARY KEY,
	type VARCHAR(40) NOT NULL,
	bucket_name varchar(100),
	path varchar(100)
);

insert into storages(type, bucket_name, path) values('STAGING', 'stage-bucket', '/stage');
insert into storages(type, bucket_name, path) values('PERMANENT', 'permanent-bucket', '/permanent');