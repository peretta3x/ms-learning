package com.epam.ms.storageservice.persistence;

import org.springframework.data.repository.ListCrudRepository;

public interface StorageRepository extends ListCrudRepository<StorageJpaEntity, Integer> {
}
