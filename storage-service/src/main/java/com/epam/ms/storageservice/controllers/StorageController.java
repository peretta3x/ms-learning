package com.epam.ms.storageservice.controllers;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.ms.storageservice.persistence.StorageJpaEntity;
import com.epam.ms.storageservice.persistence.StorageRepository;

@RestController
@RequestMapping(value = "storages")
public class StorageController {

    private final StorageRepository storageRepository;

   public StorageController(StorageRepository storageRepository) {
        this.storageRepository = storageRepository;
    }

@GetMapping(value = {"/", ""})
    public List<StorageJpaEntity> test() {
        return storageRepository.findAll();
    }
}
