package com.epam.ms.storageservice.persistence;

import jakarta.persistence.*;

@Entity(name = "Storage")
@Table(name = "storages")
public class StorageJpaEntity {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Integer id;

    @Column(name = "type", nullable = false)
    private String type;

    @Column(name = "bucket_name", nullable = false)
    private String bucketName;

    @Column(name = "path", nullable = false)
    private String path;

    public StorageJpaEntity() {
    }

    public StorageJpaEntity(Integer id, String type, String bucketName, String path) {
        this.id = id;
        this.type = type;
        this.bucketName = bucketName;
        this.path = path;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

}
