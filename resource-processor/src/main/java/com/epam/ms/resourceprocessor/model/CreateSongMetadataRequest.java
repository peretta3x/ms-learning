package com.epam.ms.resourceprocessor.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public record CreateSongMetadataRequest(
    @JsonProperty("resourceId") Integer resourceId,
    @JsonProperty("name") String name,
    @JsonProperty("artist") String artist,
    @JsonProperty("album") String album,
    @JsonProperty("length") String length,
    @JsonProperty("year") Integer year
) {

    public static CreateSongMetadataRequest of (Integer id, SongMetadata metadata) {
        return new CreateSongMetadataRequest(
                id,
                metadata.name(),
                metadata.artist(),
                metadata.album(),
                metadata.length(),
                metadata.year()
        );
    }
}
