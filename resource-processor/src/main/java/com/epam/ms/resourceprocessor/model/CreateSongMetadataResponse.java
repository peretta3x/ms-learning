package com.epam.ms.resourceprocessor.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public record CreateSongMetadataResponse(
        @JsonProperty("id") Integer id
) {
}
