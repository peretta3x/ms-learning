package com.epam.ms.resourceprocessor.model;

public record SongCreatedEvent(
        Integer id
) {
}
