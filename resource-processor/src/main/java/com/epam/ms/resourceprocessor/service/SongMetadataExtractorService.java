package com.epam.ms.resourceprocessor.service;

import com.epam.ms.resourceprocessor.model.SongMetadata;
import java.io.ByteArrayInputStream;
import java.time.LocalTime;
import java.util.Optional;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.mp3.Mp3Parser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.xml.sax.helpers.DefaultHandler;

@Service
public class SongMetadataExtractorService {

    static final Logger log = LoggerFactory.getLogger(SongMetadataExtractorService.class);

    static final String TITLE = "dc:title";
    static final String ALBUM = "xmpDM:album";
    static final String ARTIST = "xmpDM:artist";
    static final String YEAR = "xmpDM:year";
    static final String DURATION = "xmpDM:duration";
    static final String UNKNOWN = "Unknown";

    public SongMetadata extract(byte[] content)  {
        try(var inputStream = new ByteArrayInputStream(content)) {
            var handler = new DefaultHandler();
            var metadata = new Metadata();
            var parser = new Mp3Parser();
            var parseCtx = new ParseContext();

            parser.parse(inputStream, handler, metadata, parseCtx);

            var title = Optional.ofNullable(metadata.get(TITLE)).orElse(UNKNOWN);
            var album = Optional.ofNullable(metadata.get(ALBUM)).orElse(UNKNOWN);
            var artist = Optional.ofNullable(metadata.get(ARTIST)).orElse(UNKNOWN);
            var year = Optional.ofNullable(metadata.get(YEAR)).orElse("0");
            var lengthString = Optional.ofNullable(metadata.get(DURATION)).orElse("0");
            var length = getLengthAsHumanReadableTime(lengthString);

            return new SongMetadata(title, artist, album, length, Integer.valueOf(year));
        } catch (Exception e) {
            log.error("Error to extract metadata {}", e);
            throw new RuntimeException(e);
        }
    }

    private String getLengthAsHumanReadableTime(String lengthString) {
        return LocalTime.MIN.plusSeconds(Double.valueOf(lengthString).intValue()).toString();
    }
}
