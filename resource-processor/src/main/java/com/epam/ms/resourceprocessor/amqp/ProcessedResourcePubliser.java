package com.epam.ms.resourceprocessor.amqp;

import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.epam.ms.resourceprocessor.Json;
import com.epam.ms.resourceprocessor.model.SongProcessedEvent;

@Component
public class ProcessedResourcePubliser {

    private static final Logger log = LoggerFactory.getLogger(ProcessedResourcePubliser.class);
    
    private final RabbitTemplate rabbitTemplate;
    private final String processedQueue;

    public ProcessedResourcePubliser(
        final RabbitTemplate rabbitTemplate, 
        @Value("${amqp.queues.song-processed.queue}") final String processedQueue
    ) {
        this.rabbitTemplate = Objects.requireNonNull(rabbitTemplate);
        this.processedQueue = Objects.requireNonNull(processedQueue);
    }

    void publishMessage(final SongProcessedEvent event) {
        final var message = Json.writeValueAsString(event);
        log.info("[rabbitmq] [processed: %s]".formatted(message));
        this.rabbitTemplate.convertAndSend(processedQueue, message);
    }
}
