package com.epam.ms.resourceprocessor.repository;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.service.annotation.GetExchange;

public interface ResourceService {

    @GetExchange("/resources/{id}")
    byte[] getResource(@PathVariable Integer id, @RequestHeader(name = "Range", required = false) String range);

}
