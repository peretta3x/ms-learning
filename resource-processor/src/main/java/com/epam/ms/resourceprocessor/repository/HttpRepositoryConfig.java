package com.epam.ms.resourceprocessor.repository;

import java.time.Duration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.support.WebClientAdapter;
import org.springframework.web.service.invoker.HttpServiceProxyFactory;

import io.micrometer.observation.ObservationRegistry;
import reactor.util.retry.Retry;

@Configuration
public class HttpRepositoryConfig
{
    private static final int MAX_IN_MEMORY_SIZE = 5000 * 1024;

    @Bean
    ResourceService buildResourceService(
             @Value("${resource-service.base-url}") final String baseUrl,
             final ObservationRegistry observationRegistry
             ) {
        var client = WebClient.builder()
                .baseUrl(baseUrl)
                .filter(configureRetry())
                .observationRegistry(observationRegistry)
                .codecs(codecs -> codecs.defaultCodecs().maxInMemorySize(MAX_IN_MEMORY_SIZE))
                .build();
        var factory = HttpServiceProxyFactory.builder(WebClientAdapter.forClient(client)).build();

        return factory.createClient(ResourceService.class);
    }

    @Bean
    public SongService buildSongService(
            @Value("${song-service.base-url}") final String baseUrl,
            final ObservationRegistry observationRegistry
    ) {
        var client = WebClient.builder()
                .baseUrl(baseUrl)
                .filter(configureRetry())
                .observationRegistry(observationRegistry)
                .codecs(codecs -> codecs.defaultCodecs().maxInMemorySize(MAX_IN_MEMORY_SIZE))
                .build();
        var factory = HttpServiceProxyFactory.builder(WebClientAdapter.forClient(client)).build();

        return factory.createClient(SongService.class);
    }

    private static ExchangeFilterFunction configureRetry() {
        return (req, next) -> next.exchange(req)
                .retryWhen(
                        Retry.backoff(3, Duration.ofSeconds(2))
                            .jitter(0.75)
                );
    }


}
