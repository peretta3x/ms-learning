package com.epam.ms.resourceprocessor.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public record SongMetadata(
        @JsonProperty("name") String name,
        @JsonProperty("artist") String artist,
        @JsonProperty("album") String album,
        @JsonProperty("length") String length,
        @JsonProperty("year") Integer year
) {
}

