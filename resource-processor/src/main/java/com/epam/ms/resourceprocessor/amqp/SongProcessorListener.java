package com.epam.ms.resourceprocessor.amqp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import com.epam.ms.resourceprocessor.Json;
import com.epam.ms.resourceprocessor.model.CreateSongMetadataRequest;
import com.epam.ms.resourceprocessor.model.SongCreatedEvent;
import com.epam.ms.resourceprocessor.model.SongProcessedEvent;
import com.epam.ms.resourceprocessor.repository.ResourceService;
import com.epam.ms.resourceprocessor.repository.SongService;
import com.epam.ms.resourceprocessor.service.SongMetadataExtractorService;

import io.micrometer.observation.Observation;
import io.micrometer.observation.ObservationRegistry;

@Component
public class SongProcessorListener {

    private static final Logger log = LoggerFactory.getLogger(SongProcessorListener.class);

    private static final String LISTENER_ID = "songProcessorListener";

    private final SongMetadataExtractorService songMetadataExtractorService;

    private final ResourceService resourceService;

    private final SongService songService;

    private final ObservationRegistry observationRegistry;

    private final ProcessedResourcePubliser processedResourcePubliser;

    public SongProcessorListener(
            final SongMetadataExtractorService songMetadataExtractorService,
            final ResourceService resourceService,
            final SongService songService, ObservationRegistry observationRegistry,
            final ProcessedResourcePubliser processedResourcePubliser) {
        this.songMetadataExtractorService = songMetadataExtractorService;
        this.resourceService = resourceService;
        this.songService = songService;
        this.observationRegistry = observationRegistry;
        this.processedResourcePubliser = processedResourcePubliser;
    }

    @RabbitListener(id = LISTENER_ID, queues = "${amqp.queues.song-created.queue}")
    public void onSongCreatedMessage(@Payload final String message) {
        Observation.createNotStarted("process-song-created", observationRegistry)
                .lowCardinalityKeyValue("song.create.event", message)
                .observe(() -> processEvent(message));
    }

    private void processEvent(String message) {
        log.info("[message:song.listener.income] [status:completed] [payload:{}]", message);

        var songEventCreated = Json.readValue(message, SongCreatedEvent.class);

        var resourceContentResponse = resourceService.getResource(songEventCreated.id(), "bytes=0-128");
        var songMetadata = songMetadataExtractorService.extract(resourceContentResponse);

        var createSongMetadataRequest = CreateSongMetadataRequest.of(songEventCreated.id(), songMetadata);
        var createSongResponse = this.songService.createSongMetadata(createSongMetadataRequest);

        this.processedResourcePubliser.publishMessage(new SongProcessedEvent(songEventCreated.id()));
        log.info("[message:song.metadata] [status:create] [result:{}]", createSongResponse.toString());
    }

}
