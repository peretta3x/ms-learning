package com.epam.ms.resourceprocessor.repository;

import com.epam.ms.resourceprocessor.model.CreateSongMetadataRequest;
import com.epam.ms.resourceprocessor.model.CreateSongMetadataResponse;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.service.annotation.PostExchange;

public interface SongService {

    @PostExchange(value = "/songs", contentType = MediaType.APPLICATION_JSON_VALUE)
    CreateSongMetadataResponse createSongMetadata(@RequestBody CreateSongMetadataRequest request);
}
