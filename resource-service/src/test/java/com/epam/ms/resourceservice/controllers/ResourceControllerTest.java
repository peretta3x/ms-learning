package com.epam.ms.resourceservice.controllers;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.epam.ms.resourceservice.ControllerTest;
import com.epam.ms.resourceservice.model.CreateResourceOutput;
import com.epam.ms.resourceservice.service.ResourceService;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;

@ControllerTest(controllers = ResourceController.class)
class ResourceControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ResourceService resourceService;

    @Test
    void givenAnInvalidIdWhenCallGetByIdShouldReturnNotFound() throws Exception {
        final var expectedId = 1;

        when(resourceService.downloadResourceById(any(), any()))
                .thenReturn(Optional.empty());

        final var request = get("/resources/{id}", expectedId)
                .contentType(MediaType.APPLICATION_JSON);

        final var response = this.mvc.perform(request);

        response.andExpect(status().isNotFound())
                .andDo(print());

        verify(resourceService).downloadResourceById(
                argThat(id -> Objects.equals(expectedId, id)),
                argThat(range -> Objects.isNull(range))
        );
    }
     @Test
    void givenAnValidIdWhenCallGetByIdShouldReturnArrayOfBytes() throws Exception {
        final var expectedId = 1;
        final var expectedBodyResult = UUID.randomUUID().toString().getBytes();
        final var expectedRange = "bytes=0-10";

        when(resourceService.downloadResourceById(any(), any()))
                .thenReturn(Optional.of(expectedBodyResult));

        final var request = get("/resources/{id}", expectedId)
                .header("Range", expectedRange)
                .contentType(MediaType.APPLICATION_JSON);

        final var response = this.mvc.perform(request);

        final var fileContent = response.andReturn().getResponse().getContentAsByteArray();

         assertEquals(expectedBodyResult.length, fileContent.length);

        response.andExpect(status().isOk())
                .andDo(print());

         verify(resourceService).downloadResourceById(
                 argThat(id -> Objects.equals(expectedId, id)),
                 argThat(range -> Objects.equals(expectedRange, range))
         );
    }

    @Test
    void givenAValidMp3FileWhenCallCreateShouldReturnCreated() throws Exception {
        final var expectedAudioFile = new MockMultipartFile("audio_file", "audio.mp4", "audio/mpeg",  "AUDIO".getBytes());
        final var expectedId = 1;

        when(resourceService.createResource(any()))
                .thenReturn(new CreateResourceOutput(expectedId));

        final var request = multipart("/resources")
                .file(expectedAudioFile)
                .contentType(MediaType.MULTIPART_FORM_DATA);

        this.mvc.perform(request)
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", equalTo(expectedId)))
                .andDo(print());
    }

    @Test
    void givenAnInvalidFileWhenCallCreateShouldReturnBadRequest() throws Exception {
        final var expectedAudioFile = new MockMultipartFile("audio_file", "audio.txt", "text/plain",  "AUDIO".getBytes());
        final var expectedId = 1;

        when(resourceService.createResource(any()))
                .thenReturn(new CreateResourceOutput(expectedId));

        final var request = multipart("/resources")
                .file(expectedAudioFile)
                .contentType(MediaType.MULTIPART_FORM_DATA);

        this.mvc.perform(request)
                .andExpect(status().isBadRequest());
    }

}
