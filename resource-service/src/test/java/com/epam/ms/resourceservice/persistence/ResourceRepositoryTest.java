package com.epam.ms.resourceservice.persistence;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import com.epam.ms.resourceservice.IntegrationTest;

@IntegrationTest
@DataJpaTest
class ResourceRepositoryTest {

    @Autowired
    private ResourceRepository resourceRepository;

    @Test
    void repositoryInstanceShouldNotBeNull() {
        assertNotNull(this.resourceRepository);
    }

    @Test
    void givenAValidResourceEntityWhenCallSaveShouldStoreItOnDatabase() {
        final var expectedKey = UUID.randomUUID().toString().substring(4);

        assertTrue( this.resourceRepository.findAll().isEmpty());

        final var createdResource = this.resourceRepository.save(ResourceJpaEntity.newResource(expectedKey));

        assertEquals(1,  this.resourceRepository.findAll().size());

        final var actualResource = this.resourceRepository.findById(createdResource.getId());

        assertEquals(expectedKey, actualResource.get().getFileKey());
    }

    @Test
    void givenAValidListOfIdsShouldFindAllWhenCallFindAllById() {
        final var expectedKey1 = UUID.randomUUID().toString().substring(4);
        final var expectedKey2 = UUID.randomUUID().toString().substring(4);

        assertTrue( this.resourceRepository.findAll().isEmpty());

        final var resources = List.of(ResourceJpaEntity.newResource(expectedKey1), ResourceJpaEntity.newResource(expectedKey2));
        final var createdResources = this.resourceRepository.saveAll(resources);

        final var actualResult = this.resourceRepository.findAll();

        assertEquals(2,  actualResult.size());
        assertTrue(createdResources.containsAll(actualResult));
    }

    @Test
    void givenAValidListOfIdsShouldDeleteWhenCallDeleteAllById() {
        final var expectedKey1 = UUID.randomUUID().toString().substring(4);
        final var expectedKey2 = UUID.randomUUID().toString().substring(4);

        assertTrue( this.resourceRepository.findAll().isEmpty());

        final var resources = List.of(
                ResourceJpaEntity.newResource(expectedKey1),
                ResourceJpaEntity.newResource(expectedKey2)
        );

        resourceRepository.saveAll(resources);

        final var actualResult = this.resourceRepository.findAll();

        assertEquals(2,  actualResult.size());

        this.resourceRepository.deleteAllById(actualResult.stream().map(ResourceJpaEntity::getId).toList());

        assertTrue(this.resourceRepository.findAll().isEmpty());
    }

}
