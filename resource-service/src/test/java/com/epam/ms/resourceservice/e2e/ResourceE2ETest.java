package com.epam.ms.resourceservice.e2e;

import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.testcontainers.containers.localstack.LocalStackContainer.Service.S3;

import com.epam.ms.resourceservice.E2ETest;
import com.epam.ms.resourceservice.persistence.ResourceRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.containers.RabbitMQContainer;
import org.testcontainers.containers.localstack.LocalStackContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@E2ETest
@Testcontainers
public class ResourceE2ETest {

    // properties needed to authenticate on localstack
    static {
        System.setProperty("aws.accessKeyId", "123");
        System.setProperty("aws.secretAccessKey", "123");
    }

    @Autowired
    private MockMvc mvc;


    @Autowired
    private ResourceRepository resourceRepository;

    @Container
    private static final PostgreSQLContainer POSTGRESQL_CONTAINER = new PostgreSQLContainer("postgres:14-alpine")
            .withPassword("rs-test")
            .withUsername("rs-test")
            .withDatabaseName("resource-service-db");

    @Container
    private static final RabbitMQContainer RABBIT_MQ_CONTAINER = new RabbitMQContainer("rabbitmq:3-management")
            .withVhost("/")
            .withAccessToHost(true);


    @Container
    public static final LocalStackContainer LOCAL_STACK_CONTAINER = new LocalStackContainer("1.4")
            .withServices(S3)
            .withExposedPorts(4566);

    @DynamicPropertySource
    public static void setDatasourceProperties(final DynamicPropertyRegistry registry) {
        registry.add("postgres.port", () -> POSTGRESQL_CONTAINER.getMappedPort(5432));
        registry.add("amqp.port", () -> RABBIT_MQ_CONTAINER.getMappedPort(5672));
        registry.add("localstack.port", () -> LOCAL_STACK_CONTAINER.getMappedPort(4566));
    }

    @Test
    void givenAValidFileWhenCallCreateEndpointShouldCreateResource() throws Exception {
        final var expectedAudioFile = new MockMultipartFile("audio_file", "audio.mp4", "audio/mpeg",  "AUDIO".getBytes());

        assertTrue(this.resourceRepository.findAll().isEmpty());

        final var request = multipart("/resources")
                .file(expectedAudioFile)
                .contentType(MediaType.MULTIPART_FORM_DATA);

        this.mvc.perform(request)
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", notNullValue()))
                .andDo(print());

        assertEquals(1, this.resourceRepository.findAll().size());
    }
}
