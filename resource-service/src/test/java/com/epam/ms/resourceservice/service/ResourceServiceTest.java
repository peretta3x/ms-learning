package com.epam.ms.resourceservice.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockMultipartFile;

import com.epam.ms.resourceservice.model.Resource;
import com.epam.ms.resourceservice.persistence.ResourceJpaEntity;
import com.epam.ms.resourceservice.persistence.ResourceRepository;
import com.epam.ms.resourceservice.storage.ResourceStorageService;

@ExtendWith(MockitoExtension.class)
class ResourceServiceTest {

    @InjectMocks
    private ResourceService resourceService;

    @Mock
    private ResourceRepository resourceRepository;

    @Mock
    private ResourceStorageService resourceStorageService;

    @Mock
    private ResourceMessagePublisher resourceMessagePublisher;

    @Mock
    private StorageDetailsService storageDetailsService;

    @Test
    public void givenAnExistingIdShouldReturnResource() {
        final var expectedId = 1;
        final var expectedFileKey = "file-key";
        final var expectedFileName = "audio.mp3";
        final var expectedContent = expectedFileName.getBytes();
        final var storageDetails = new StorageDetailResponse(1L, "PERMANENT", "bucket", "staging");

        when(resourceRepository.findById(expectedId))
                .thenReturn(Optional.of(new ResourceJpaEntity(expectedId, expectedFileKey)));

        when(resourceStorageService.download(expectedFileKey, null, storageDetails.bucketName()))
                .thenReturn(new Resource(expectedFileKey, expectedFileName, (long) expectedFileName.length(), expectedContent));

        final var expectedResult = resourceService.downloadResourceById(expectedId, null);

        assertTrue(expectedResult.isPresent());
        assertEquals(expectedContent, expectedResult.get());

    }

    @Test
    public void givenAnInvalidIdShouldReturnEmptyContent() {
        final var expectedId = 1;
        when(resourceRepository.findById(expectedId))
                .thenReturn(Optional.empty());

        final var expectedResult = resourceService.downloadResourceById(expectedId, null);

        assertTrue(expectedResult.isEmpty());
    }

    @Test
    public void givenAListOfIdShouldDeleteTheResources() {
        final var expectedId1 = 1;
        final var expectedId2 = 2;

        final var expectedIds = List.of(expectedId1, expectedId2);

        final var expectedFileKey1 = "file-key1";
        final var expectedFileKey2 = "file-key2";

        final var resourceEntities = List.of(
                new ResourceJpaEntity(expectedId1, expectedFileKey1),
                new ResourceJpaEntity(expectedId2, expectedFileKey2)
        );


        when(resourceRepository.findAllById(expectedIds))
                .thenReturn(resourceEntities);

        doNothing().when(resourceRepository).deleteAllById(expectedIds);


        resourceService.deleteResources(expectedIds);

        verify(resourceStorageService, times(1)).delete(expectedFileKey1);
        verify(resourceStorageService, times(1)).delete(expectedFileKey2);
    }

    @Test
    public void givenAnValidMultipartFileShouldCreateNewResource() {
        final var expectedFileName = "audio.mp3";
        final var expectedKey = "file-key";
        final var expectedResourceEntity = ResourceJpaEntity.newResource(expectedKey);
        expectedResourceEntity.setId(1);
        final var storageDetails = new StorageDetailResponse(1L, "STAGING", "bucket", "staging");

        final var mockMultipartFile = new MockMultipartFile(expectedFileName, expectedFileName.getBytes());

        when(storageDetailsService.getStorageByType(anyString()))
                .thenReturn(Optional.of(storageDetails));

        when(resourceStorageService.upload(mockMultipartFile, storageDetails.bucketName()))
                .thenReturn(expectedKey);

        when(resourceRepository.save(any(ResourceJpaEntity.class)))
                .thenReturn(expectedResourceEntity);


        resourceService.createResource(mockMultipartFile);

        verify(resourceMessagePublisher).publishMessage(argThat(actualEvent ->
                Objects.equals(actualEvent.id(),expectedResourceEntity.getId()))
        );
    }

}
