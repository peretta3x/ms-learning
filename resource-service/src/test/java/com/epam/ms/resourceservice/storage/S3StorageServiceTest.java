package com.epam.ms.resourceservice.storage;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.InputStream;
import java.util.Objects;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.mock.web.MockMultipartFile;

import software.amazon.awssdk.core.ResponseInputStream;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.*;

class S3StorageServiceTest {

    private S3StorageService s3StorageService;

    private S3Client s3Client = Mockito.mock(S3Client.class);

    private String bucketName = "testBucket";

    public S3StorageServiceTest() {
        this.s3StorageService = new S3StorageService(s3Client, bucketName);
    }

    @Test
    public void givenAValidKeyShouldDeleteResourceWhenCallDelete() {
        final var expectedKey = "key";

        when(s3Client.deleteObject(any(DeleteObjectRequest.class)))
                .thenReturn(DeleteObjectResponse.builder().build());

        s3StorageService.delete(expectedKey);


        verify(s3Client).headBucket((HeadBucketRequest) argThat(actualRequest ->
                Objects.equals(bucketName, ((HeadBucketRequest) actualRequest).bucket())
        ));

        verify(s3Client).deleteObject((DeleteObjectRequest) argThat(actualRequest ->
                Objects.equals(bucketName, ((DeleteObjectRequest) actualRequest).bucket())
                && Objects.equals(expectedKey, ((DeleteObjectRequest) actualRequest).key())
        ));
    }


    @Test
    public void givenAValidMultipartFileShouldSaveFileWhenCallUpload() {
        final var expectedFileName = "audio.mp3";
        final var mockMultipartFile = new MockMultipartFile(expectedFileName, expectedFileName.getBytes());

        when(s3Client.putObject(
                any(PutObjectRequest.class), any(RequestBody.class)))
                .thenReturn(PutObjectResponse.builder().build());

        s3StorageService.upload(mockMultipartFile, "");

        verify(s3Client).headBucket((HeadBucketRequest) argThat(actualRequest ->
                Objects.equals(bucketName, ((HeadBucketRequest) actualRequest).bucket())
        ));

        verify(s3Client).putObject(
                (PutObjectRequest) argThat(actualRequest ->
                            Objects.equals(bucketName, ((PutObjectRequest) actualRequest).bucket())
                            && Objects.nonNull(((PutObjectRequest) actualRequest).key())),
                (RequestBody) argThat(request ->
                            Objects.equals(
                                    ((RequestBody)request).optionalContentLength().get(),
                                    RequestBody.fromBytes(expectedFileName.getBytes()).optionalContentLength().get()
                            )
                )
        );
    }

    @Test
    public void givenValidIdAndNullRangeWhenCallDownloadShouldReturnResource() {

        final var expectedKey = "file-key";

        ResponseInputStream<GetObjectResponse> getObjectResponseResponseInputStream = new ResponseInputStream<>(
                GetObjectResponse.builder().contentLength(1L).build(),
                InputStream.nullInputStream()
        );

        when(s3Client.getObject(any(GetObjectRequest.class)))
                .thenReturn(getObjectResponseResponseInputStream);

        final var actualResult = s3StorageService.download(expectedKey, null, "PERMANENT");

        assertEquals(expectedKey, actualResult.key());
        assertEquals("%s.mp3".formatted(expectedKey), actualResult.fileName());

        verify(s3Client).getObject((GetObjectRequest) argThat(actualRequest ->
                Objects.equals(bucketName, ((GetObjectRequest) actualRequest).bucket())
                && Objects.equals(expectedKey, ((GetObjectRequest) actualRequest).key())
        ));
    }

    @Test
    public void givenValidIdAndRangeWhenCallDownloadShouldReturnResource() {

        final var expectedKey = "file-key";
        final var expectedRange = "0-128";

        final var getObjectResponseResponseInputStream = new ResponseInputStream<>(
                GetObjectResponse.builder().contentLength(1L).build(),
                InputStream.nullInputStream()
        );

        when(s3Client.getObject(any(GetObjectRequest.class)))
                .thenReturn(getObjectResponseResponseInputStream);

        final var actualResult = s3StorageService.download(expectedKey, expectedRange, "PERMANENT");

        assertEquals(expectedKey, actualResult.key());
        assertEquals("%s.mp3".formatted(expectedKey), actualResult.fileName());

        verify(s3Client).getObject((GetObjectRequest) argThat(actualRequest ->
                Objects.equals(bucketName, ((GetObjectRequest) actualRequest).bucket())
                        && Objects.equals(expectedKey, ((GetObjectRequest) actualRequest).key())
                        && Objects.equals(expectedRange, ((GetObjectRequest) actualRequest).range())
        ));
    }

}
