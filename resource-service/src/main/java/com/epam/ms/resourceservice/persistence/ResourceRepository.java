package com.epam.ms.resourceservice.persistence;

import org.springframework.data.repository.ListCrudRepository;

public interface ResourceRepository extends ListCrudRepository<ResourceJpaEntity, Integer> {
}
