package com.epam.ms.resourceservice.service;

import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

import com.epam.ms.resourceservice.Json;
import com.epam.ms.resourceservice.configuration.SongCreatedQueue;
import com.epam.ms.resourceservice.configuration.properties.QueueProperties;
import com.epam.ms.resourceservice.model.CreateResourceOutput;

@Component
public class ResourceMessagePublisher implements MessagePublisher<CreateResourceOutput> {

    private static final Logger log = LoggerFactory.getLogger(ResourceMessagePublisher.class);

    private final RabbitTemplate rabbitTemplate;
    private final QueueProperties queueProperties;

    public ResourceMessagePublisher(RabbitTemplate rabbitTemplate, @SongCreatedQueue QueueProperties queueProperties) {
        this.rabbitTemplate = Objects.requireNonNull(rabbitTemplate);
        this.queueProperties = Objects.requireNonNull(queueProperties);
    }

    @Override
    public void publishMessage(final CreateResourceOutput createResourceOutput) {
        var message = Json.writeValueAsString(createResourceOutput);
        log.info("[rabbitmq] [message: %s]".formatted(message));
        this.rabbitTemplate.convertAndSend(queueProperties.getQueue(), message);
    }
}
