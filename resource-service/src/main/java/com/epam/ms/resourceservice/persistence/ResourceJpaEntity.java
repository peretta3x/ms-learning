package com.epam.ms.resourceservice.persistence;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity(name = "Resource")
@Table(name = "resources")
public class ResourceJpaEntity {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Integer id;

    @Column(name = "file_key", nullable = false)
    private String fileKey;

    public ResourceJpaEntity() {
    }

    public ResourceJpaEntity(Integer id, String fileKey) {
        this.id = id;
        this.fileKey = fileKey;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFileKey() {
        return fileKey;
    }

    public void setFileKey(String fileKey) {
        this.fileKey = fileKey;
    }

    public static ResourceJpaEntity newResource(final String fileKey) {
        return new ResourceJpaEntity(null, fileKey);
    }
}
