package com.epam.ms.resourceservice.model;

public record CreateResourceOutput(
        Integer id
) {
}
