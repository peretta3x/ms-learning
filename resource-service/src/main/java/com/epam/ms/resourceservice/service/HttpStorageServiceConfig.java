package com.epam.ms.resourceservice.service;

import java.time.Duration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.support.WebClientAdapter;
import org.springframework.web.service.invoker.HttpServiceProxyFactory;

import io.micrometer.observation.ObservationRegistry;
import reactor.util.retry.Retry;


@Configuration
class HttpStorageServiceConfig {
    
    @Bean
    StorageService buildStorageService(
            @Value("${storage-service.base-url}") final String baseUrl,
            final ObservationRegistry observationRegistry
    ) {
        var client = WebClient.builder()
                .baseUrl(baseUrl)
                .filter(configureRetry())
                .observationRegistry(observationRegistry)
                .build();
        var factory = HttpServiceProxyFactory.builder(WebClientAdapter.forClient(client)).build();

        return factory.createClient(StorageService.class);
    }

    private static ExchangeFilterFunction configureRetry() {
        return (req, next) -> next.exchange(req)
                .retryWhen(
                        Retry.backoff(3, Duration.ofSeconds(2))
                            .jitter(0.75)
                );
    }

}
