package com.epam.ms.resourceservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ResourceServiceApplication {

    public static void main(String[] args) {
        System.out.println(System.getenv("AWS_SECRET_ACCESS_KEY"));
        System.out.println(System.getenv("AWS_ACCESS_KEY_ID"));
        SpringApplication.run(ResourceServiceApplication.class, args);
    }

}
