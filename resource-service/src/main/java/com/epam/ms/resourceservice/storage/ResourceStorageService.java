package com.epam.ms.resourceservice.storage;

import org.springframework.web.multipart.MultipartFile;

import com.epam.ms.resourceservice.model.Resource;

public interface ResourceStorageService {

    String upload(MultipartFile file, String bucketName);

    Resource download(String id, String range, String bucketName);

    void delete(String key);

    void moveTo(String key, String sourceBucketName, String destinationBucketName);

}
