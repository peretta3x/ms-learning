package com.epam.ms.resourceservice.configuration;

import org.springframework.amqp.core.*;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.epam.ms.resourceservice.configuration.properties.QueueProperties;

@Configuration
public class AmqpConfig {

    @Bean
    @SongCreatedQueue
    @ConfigurationProperties("amqp.queues.song-created")
    QueueProperties songCreatedQueueProperties() {
        return new QueueProperties();
    }

    @Bean
    @SongProcessedQueue
    @ConfigurationProperties("amqp.queues.song-processed")
    QueueProperties songProcessedQueueProperties() {
        return new QueueProperties();
    }

    static class Admin {

        @Bean
        @SongEvents
        Exchange songEventsExchange(@SongCreatedQueue QueueProperties props) {
            return new DirectExchange(props.getExchange());
        }

        @Bean
        @SongCreatedQueue
        Queue songCreatedQueue(@SongCreatedQueue QueueProperties props) {
            return new Queue(props.getQueue());
        }

        @Bean
        @SongCreatedQueue
        Binding songCreatedBinding(
            @SongEvents DirectExchange exchange,
            @SongCreatedQueue Queue queue,
            @SongCreatedQueue QueueProperties props
        ) {
            return BindingBuilder.bind(queue).to(exchange).with(props.getRoutingKey());
        }

        @Bean
        @SongProcessedQueue
        Queue songProcessedQueue(@SongProcessedQueue QueueProperties props) {
            return new Queue(props.getQueue());
        }

        @Bean
        @SongProcessedQueue
        Binding songProcessedBinding(
            @SongEvents DirectExchange exchange,
            @SongProcessedQueue Queue queue,
            @SongProcessedQueue QueueProperties props
        ) {
            return BindingBuilder.bind(queue).to(exchange).with(props.getRoutingKey());
        }
    }

}
