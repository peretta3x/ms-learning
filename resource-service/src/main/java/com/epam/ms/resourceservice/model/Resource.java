package com.epam.ms.resourceservice.model;

public record Resource(
        String key,
        String fileName,
        Long contentLength,
        byte[] content
) {
}
