package com.epam.ms.resourceservice.storage;

public record DownloadFileRange(
        int start,
        int end
) {
}
