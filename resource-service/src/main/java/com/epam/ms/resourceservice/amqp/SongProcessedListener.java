package com.epam.ms.resourceservice.amqp;

import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import com.epam.ms.resourceservice.Json;
import com.epam.ms.resourceservice.model.ProcessedResourceInput;
import com.epam.ms.resourceservice.service.ResourceService;

@Component
public class SongProcessedListener {

    private static final Logger log = LoggerFactory.getLogger(SongProcessedListener.class);

    private final ResourceService resourceService;

    public SongProcessedListener(ResourceService resourceService) {
        this.resourceService = Objects.requireNonNull(resourceService);
    }

    @RabbitListener(id = "songProcessedListener", queues = "${amqp.queues.song-processed.queue}")
    void onSongProcessedMessage(@Payload final String message) {
        log.debug("song processed listener input {}", message);
        final var processedInput = Json.readValue(message, ProcessedResourceInput.class);
        this.resourceService.moveToPermanent(processedInput.id());
    }
    
}
