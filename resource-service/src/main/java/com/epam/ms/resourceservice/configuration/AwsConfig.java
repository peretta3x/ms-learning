package com.epam.ms.resourceservice.configuration;

import java.net.URI;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;

@Configuration
public class AwsConfig {

    @Value("${aws.s3.endpoint-url}")
    private String endpointUrl;

    @Value("${aws.s3.bucket-name}")
    private String bucketName;

    @Bean
    S3Client amazonS3() {
        S3Client s3 = S3Client.builder()
                .endpointOverride(URI.create(endpointUrl))
                .region(Region.US_EAST_1)
                .forcePathStyle(true)
                .build();
        return s3;
    }

    @Bean
    String bucketName() {
        return this.bucketName;
    }
}
