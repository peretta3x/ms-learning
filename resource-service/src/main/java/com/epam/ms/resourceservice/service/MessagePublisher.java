package com.epam.ms.resourceservice.service;

public interface MessagePublisher<T> {

    void publishMessage(T message);
}
