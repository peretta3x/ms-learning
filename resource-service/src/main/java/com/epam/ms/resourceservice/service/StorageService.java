package com.epam.ms.resourceservice.service;

import java.util.List;

import org.springframework.web.service.annotation.GetExchange;

public interface StorageService {

    @GetExchange("/storages/")
    List<StorageDetailResponse> getStorageDetails();
}
