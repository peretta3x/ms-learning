package com.epam.ms.resourceservice.controllers;

import com.epam.ms.resourceservice.model.CreateResourceOutput;
import com.epam.ms.resourceservice.model.DeleteResourceOutput;
import com.epam.ms.resourceservice.service.ResourceService;
import java.net.URI;
import java.util.Arrays;
import java.util.Objects;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping(value = "resources")
public class ResourceController {

    private static final String AUDIO_MPEG = "audio/mpeg";

    private final ResourceService resourceService;

    public ResourceController(final ResourceService resourceService) {
        this.resourceService = Objects.requireNonNull(resourceService);
    }

    @PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<CreateResourceOutput> create(@RequestParam(name = "audio_file") final MultipartFile audioFile) {

        if (!AUDIO_MPEG.equals(audioFile.getContentType())) {
            return ResponseEntity.badRequest().build();
        }

        var createdResource = this.resourceService.createResource(audioFile);

        return ResponseEntity
                .created(URI.create("/resources/" + createdResource.id()))
                .body(createdResource);
    }

    @GetMapping(value = "{id}")
    ResponseEntity<byte[]> getById(
            @PathVariable(name = "id") final Integer id,
            @RequestHeader(name = "Range", required = false) final String range
    ) {
        var resourceContent = this.resourceService.downloadResourceById(id, range);

        if (resourceContent.isPresent()) {
            return ResponseEntity
                    .ok()
                    .contentType(MediaType.valueOf(AUDIO_MPEG))
                    .contentLength(resourceContent.get().length)
                    .body(resourceContent.get());
        }

        return ResponseEntity.notFound().build();
    }

    @DeleteMapping
    public ResponseEntity<DeleteResourceOutput> deleteResources(@RequestParam(name = "id") final String ids) {
        var resourceIds = Arrays.stream(ids.split(","))
                .map(Integer::valueOf)
                .toList();

        this.resourceService.deleteResources(resourceIds);

        var output = new DeleteResourceOutput(resourceIds);
        return ResponseEntity.ok(output);
    }
}
