package com.epam.ms.resourceservice.service;

import java.util.Collection;
import java.util.Objects;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.epam.ms.resourceservice.model.CreateResourceOutput;
import com.epam.ms.resourceservice.persistence.ResourceJpaEntity;
import com.epam.ms.resourceservice.persistence.ResourceRepository;
import com.epam.ms.resourceservice.storage.ResourceStorageService;

@Service
public class ResourceService {

    private static final Logger log = LoggerFactory.getLogger(ResourceService.class);

    private static final String STAGING = "STAGING";
    private static final String PERMANENT = "PERMANENT";

    private final ResourceRepository resourceRepository;
    private final ResourceStorageService resourceStorageService;
    private final MessagePublisher<CreateResourceOutput> messagePublisher;
    private final StorageDetailsService storageDetailsService;

    public ResourceService(
            final ResourceRepository resourceRepository,
            final ResourceStorageService resourceStorageService,
            final MessagePublisher<CreateResourceOutput> messagePublisher,
            final StorageDetailsService storageDetailsService) {
        this.resourceRepository = Objects.requireNonNull(resourceRepository);
        this.resourceStorageService = Objects.requireNonNull(resourceStorageService);
        this.messagePublisher = Objects.requireNonNull(messagePublisher);
        this.storageDetailsService = Objects.requireNonNull(storageDetailsService);
    }

    public CreateResourceOutput createResource(final MultipartFile audioFile) {
        final var storageDetails = this.storageDetailsService.getStorageByType(STAGING).get();
        final var key = this.resourceStorageService.upload(audioFile, storageDetails.bucketName());
        final var newResource = this.resourceRepository.save( ResourceJpaEntity.newResource(key));
        final var output = new CreateResourceOutput(newResource.getId());

        this.messagePublisher.publishMessage(output);

        return output;
    }

    public Optional<byte[]> downloadResourceById(final Integer id, final String range) {
        log.debug("Download audio with id {}", id);
        final var optionalResource = this.resourceRepository.findById(id);

        if (optionalResource.isPresent()) {
            final var storageDetails = this.storageDetailsService.getStorageByType(STAGING).get();
            var resource = this.resourceStorageService.download(
                optionalResource.get().getFileKey(), 
                range, 
                storageDetails.bucketName()
            );
            return Optional.of(resource.content());
        }

        return Optional.empty();
    }

    public void deleteResources(final Collection<Integer> ids) {
        var resources = this.resourceRepository.findAllById(ids);

        resources.forEach(r -> resourceStorageService.delete(r.getFileKey()));

        var idsToDelete = resources.stream().map(ResourceJpaEntity::getId).toList();

        this.resourceRepository.deleteAllById(idsToDelete);
    }

    public void moveToPermanent(Integer id) {
        log.debug("Moving song with id {} to permantent bucket", id);
        final var optionalResource = this.resourceRepository.findById(id);
        if (optionalResource.isPresent()) {
            final var resourceKey = optionalResource.get().getFileKey();
            final var stagingStorageDetails = this.storageDetailsService.getStorageByType(STAGING).get();
            final var permanentStorageDetails = this.storageDetailsService.getStorageByType(PERMANENT).get();
            
            try {     
                this.resourceStorageService.moveTo(
                    resourceKey, 
                    stagingStorageDetails.bucketName(), 
                    permanentStorageDetails.bucketName()
                );
            } catch (Exception e) {
                log.error("Error to move song to permanent", e);
            }

        }

    }
}
