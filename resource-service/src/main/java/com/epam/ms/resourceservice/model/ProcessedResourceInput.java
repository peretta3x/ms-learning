package com.epam.ms.resourceservice.model;

public record ProcessedResourceInput(
        Integer id
) {
}
