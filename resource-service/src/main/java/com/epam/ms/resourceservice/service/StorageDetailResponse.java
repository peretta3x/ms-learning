package com.epam.ms.resourceservice.service;

public record StorageDetailResponse(
    Long id,
    String type,
    String bucketName,
    String path
) {
    
}
