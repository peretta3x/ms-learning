package com.epam.ms.resourceservice.storage;

import java.io.IOException;
import java.util.Objects;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.epam.ms.resourceservice.model.Resource;

import software.amazon.awssdk.core.ResponseInputStream;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.*;

@Service
public class S3StorageService implements ResourceStorageService {

    private static final Logger log = LoggerFactory.getLogger(S3StorageService.class);

    private final S3Client amazonS3;

    private final String bucketName;

    public S3StorageService(S3Client amazonS3, String bucketName) {
        this.amazonS3 = amazonS3;
        this.bucketName = bucketName;

        initializeBucket();
    }

    @Override
    public String upload(final MultipartFile file, String bucketName) {
        createBucketIfNotExists(bucketName);

        var key = UUID.randomUUID().toString();
        var request = PutObjectRequest.builder().bucket(bucketName).key(key).build();

        try {
            amazonS3.putObject(request, RequestBody.fromBytes(file.getBytes()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return key;
    }

    @Override
    public Resource download(String id, String range, String bucketName) {
        ResponseInputStream<GetObjectResponse> s3Object;
        GetObjectRequest request;
        if (Objects.nonNull(range)) {
            request = GetObjectRequest.builder()
                    .bucket(bucketName)
                    .key(id)
                    .range(range)
                    .build();
        } else {
            request = GetObjectRequest.builder()
                    .bucket(bucketName)
                    .key(id)
                    .build();
        }

        s3Object = amazonS3.getObject(request);
        var filename = id + "." + "mp3";
        var contentLength = s3Object.response().contentLength();
        try {
            return new Resource(id, filename, Long.valueOf(contentLength), s3Object.readAllBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void delete(String key) {
        var deleteRequest = DeleteObjectRequest.builder()
                .bucket(bucketName)
                .key(key)
                .build();
        this.amazonS3.deleteObject(deleteRequest);
    }

    private void initializeBucket() {
        createBucketIfNotExists(bucketName);
    }

    private void createBucketIfNotExists(final String bucketName) {
        if (!bucketExists(bucketName)) {
            var bucketRequest = CreateBucketRequest.builder()
                    .bucket(bucketName)
                    .build();
            amazonS3.createBucket(bucketRequest);
        }
    }

    private boolean bucketExists(final String bucketName) {
        var headBucketRequest = HeadBucketRequest.builder()
                .bucket(bucketName)
                .build();
        try {
            amazonS3.headBucket(headBucketRequest);
            return true;
        } catch (NoSuchBucketException e) {
            return false;
        }
    }

    @Override
    public void moveTo(String key, String sourceBucketName, String destinationBucketName) {
        createBucketIfNotExists(destinationBucketName);

        final var copyObjectRequest = CopyObjectRequest.builder()
            .sourceBucket(bucketName)
            .destinationBucket(destinationBucketName)
            .sourceKey(key)
            .destinationKey(key)
            .build();
        
        log.debug("Coping file from {} to {}", "%s/%s".formatted(sourceBucketName, key), "%s/%s".formatted(destinationBucketName, key));
        
        this.amazonS3.copyObject(copyObjectRequest);
        this.delete(key);
        
        log.debug("File moved to {} successfully", destinationBucketName);
        
    }
}
