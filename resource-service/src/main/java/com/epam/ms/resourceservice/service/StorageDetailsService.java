package com.epam.ms.resourceservice.service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.client.circuitbreaker.CircuitBreakerFactory;
import org.springframework.stereotype.Service;

@Service
public class StorageDetailsService {

    private final Logger log = LoggerFactory.getLogger(StorageDetailsService.class);
    
    private final StorageService storageService;
    private final CircuitBreakerFactory circuitBreakerFactory;

    public StorageDetailsService(final StorageService storageService, final CircuitBreakerFactory circuitBreakerFactory) {
        this.storageService = Objects.requireNonNull(storageService);
        this.circuitBreakerFactory = Objects.requireNonNull(circuitBreakerFactory);
    }

    public Optional<StorageDetailResponse> getStorageByType(final String type) {
        Objects.requireNonNull(type, "storage details type is mandatory");

        final var storageDetails = this.circuitBreakerFactory.create("storageDetails")
            .run(() -> this.storageService.getStorageDetails(), throwable -> getDefaultStorageDetails());

        return storageDetails.stream()
            .filter(storageDetail -> type.equals(storageDetail.type()))
            .findFirst();
    }

    private List<StorageDetailResponse> getDefaultStorageDetails() {
        log.debug("Calling callback function to get storage details");
        return List.of(
            new StorageDetailResponse(1L, "STAGING", "stage-bucket", "/permanent"),
            new StorageDetailResponse(1L, "PERMANENT", "permanent-bucket", "/stage")
        );
    }
}
