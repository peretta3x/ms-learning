package com.epam.ms.resourceservice.model;

import java.util.List;

public record DeleteResourceOutput(
        List<Integer> ids
) {
}
