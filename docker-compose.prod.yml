version: '3.1'

services:
  resource_service_db:
    image: postgres:14-alpine
    container_name: resource_service_db
    environment:
      POSTGRES_DB: resource-service-db
      POSTGRES_PASSWORD: rs-pass
      POSTGRES_USER: rs-user
    ports:
      - "5432:5432"
    networks:
      - ms_learning

  song_service_db:
    image: postgres:14-alpine
    container_name: song_service_db
    environment:
      POSTGRES_DB: song-service-db
      POSTGRES_PASSWORD: ss-pass
      POSTGRES_USER: ss-user
    ports:
      - "54322:5432"
    networks:
      - ms_learning

  storage_service_db:
    image: postgres:14-alpine
    container_name: storage_service_db
    environment:
      POSTGRES_DB: storage-service-db
      POSTGRES_PASSWORD: ss-pass
      POSTGRES_USER: ss-user
    ports:
      - "5433:5432"
    networks:
      - ms_learning

  localstack:
    container_name: ms_localstack
    image: localstack/localstack
    ports:
      - "4566:4566"
      - "8055:8080"
    environment:
      - SERVICES=s3
      - DEBUG=1
    networks:
      - ms_learning

  rabbitmq:
    container_name: ms-learning-rabbitmq
    image: rabbitmq:management-alpine
    ports:
      - "15672:15672"
      - "5672:5672"
    volumes:
      - ./rabbitmq/rabbitmq.conf:/etc/rabbitmq/rabbitmq.conf:ro
      - ./rabbitmq/definitions.json:/etc/rabbitmq/definitions.json:ro
    networks:
      - ms_learning

  zipkin:
    container_name: zipkin
    image: openzipkin/zipkin
    extra_hosts: ['host.docker.internal:host-gateway']
    restart: always
    ports:
      - "9411:9411"
    networks:
      - ms_learning

  prometheus:
    container_name: prometheus
    image: prom/prometheus
    extra_hosts: [ 'host.docker.internal:host-gateway' ]
    restart: always
    command:
        - --enable-feature=exemplar-storage
        - --config.file=/etc/prometheus/prometheus.yml
    volumes:
        - ./config/prometheus/prometheus.yml:/etc/prometheus/prometheus.yml:ro
    ports:
        - 9090:9090
    networks:
      - ms_learning
  grafana:
    container_name: grafana
    image: grafana/grafana
    extra_hosts: [ 'host.docker.internal:host-gateway' ]
    restart: always
    environment:
        - GF_AUTH_ANONYMOUS_ENABLED=true
        - GF_AUTH_ANONYMOUS_ORG_ROLE=Admin
        - GF_AUTH_DISABLE_LOGIN_FORM=true
    volumes:
        - ./config/grafana/provisioning/datasources:/etc/grafana/provisioning/datasources:ro
        - ./config/grafana/provisioning/dashboards:/etc/grafana/provisioning/dashboards:ro
    ports:
        - 3000:3000
    networks:
      - ms_learning

  eureka-server:
    container_name: eureka-server
    image: rafaelperetta/eureka-server:0.0.1-SNAPSHOT
    ports:
      - "8761:8761"
    networks:
      - ms_learning

  api-gateway:
    container_name: api-gateway
    image: rafaelperetta/gateway:0.0.1-SNAPSHOT
    env_file:
      - ./gateway/.env
    ports:
      - "8760:8760"
    networks:
      - ms_learning

  config-server:
    container_name: config-server
    image: rafaelperetta/config-server:0.0.1-SNAPSHOT
    ports:
      - "8888:8888"
    networks:
      - ms_learning

  resource-service:
    container_name: resource-service
    image: rafaelperetta/resource-service:0.0.1-SNAPSHOT
    env_file:
      - ./resource-service/.env
    ports:
      - "8080:8080"
    networks:
      - ms_learning
    depends_on:
      - eureka-server
      - resource_service_db
      - localstack
      - rabbitmq

  resource-processor:
    container_name: resource-processor
    image: rafaelperetta/resource-processor:0.0.1-SNAPSHOT
    env_file:
      - ./resource-processor/.env
    ports:
      - "8083:8083"
    networks:
      - ms_learning
    depends_on:
      - rabbitmq
      - resource-service
      - song-service
      - config-server

  song-service:
    container_name: song-service
    image: rafaelperetta/song-service:0.0.1-SNAPSHOT
    env_file:
      - ./song-service/.env
    ports:
      - "8082:8082"
    networks:
      - ms_learning
    depends_on:
      - eureka-server
      - song_service_db

  storage-service:
    container_name: storage-service
    image: rafaelperetta/storage-service:0.0.1-SNAPSHOT
    env_file:
      - ./storage-service/.env
    ports:
      - "8089:8089"
    networks:
      - ms_learning
    depends_on:
      - eureka-server
      - storage_service_db

networks:
  ms_learning:
